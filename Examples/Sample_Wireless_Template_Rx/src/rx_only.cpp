/* Author: Sam Cristall
 * License: GPLv3
 * Edited by: Thia Wyrod
 */
#include <Thread.h>
#include "WirelessThreads.h"

/* This function defines the thread that will be responsible for emptying the rx
 * buffer by executing your program/protocol logic.
 */
void* worker_thread(void* arg)
{
  auto* buffers = reinterpret_cast<WirelessBuffers*>(arg);
  auto& rx_buffer = buffers->rx_buffer;
  Packet pkt;
  while (true) {
    if (rx_buffer.HasData()) {
      pkt = rx_buffer.Peek();
      rx_buffer.Pop();
      //do_something with the popped-off packet!
    }
  }
  return NULL;
}

static WirelessBuffers data_buffers;
void* (*const thrd_funcs[])(void*) = { wireless_thread, worker_thread };

/* Set up and run all secondary threads, then sleep */
int main() {
  sys_clock_init(kCrysFreq, kSystemF);
  size_t curr_thrd_num = 1;
  for (auto thrd_func : thrd_funcs) {
    thread_setup(thrd_func, &data_buffers, curr_thrd_num);
    thread_run(curr_thrd_num);
    ++curr_thrd_num;
  }
  return 0;
}
