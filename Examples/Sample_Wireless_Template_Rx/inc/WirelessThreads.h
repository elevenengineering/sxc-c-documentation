/* Author: Sam Cristall
 * License: GPLv3
 */

#include <SXC.h>
#include <XPD.h>
#include "CRC.h"
#include "FEC.h"
#include "GinsengRadio.h"
#include "Wireless.h"
#include "time_funcs.h"
#include "WirelessConfig.h"

/* This function defines the thread that will be responsible for filling and
 * emptying the rx and tx buffers respectively by listening on the radio/BBU and
 * transmitting data. Only modify this if you are feeling brave!
 */
inline void* wireless_thread(void* arg) {
  auto* buffers = reinterpret_cast<WirelessBuffers*>(arg);
  auto& tx_buffer = buffers->tx_buffer;
  auto& rx_buffer = buffers->rx_buffer;

  RadioPort radio_port;
  RadioBbuPort bbu_port;
  RadioSpi spi(kSysFreq);
  RadioBbu bbu(kSysFreq, kRadioStartWord);
  Crc16Lfsr crc16;
  FecLfsr fec;

  auto chip_select =
     MakeOutputPin(radio_port, kRadioChipSelectPinNumber, true);
  auto bbu_pin = MakeOutputPin(bbu_port, kBbuDirectionPinNumber, true);
  auto spi_with_cs = MakeSpiWithChipSelect(spi, chip_select); 
  auto radio = MakeGinsengRadio(spi_with_cs, bbu, radio_port, bbu_pin);
  auto wireless = MakeWireless(bbu, crc16, fec, kRadioStartWord);

  while (true) {
    bool packet_present = false;

    radio.ChangeChannel(0);
    radio.SwitchToReceive();

    auto receive_timer = MakeSoftTimer(bbu);
    receive_timer.Start(kReceiveTimeout);

    // Here we start receiving a packet, it is of the following form:
    // 0x5555 preamble
    // 0x5555 
    // 0x271B start word <--- These are handled by the stream acquire
    // *** CRC initialized here to 0x1021
    // rx_ack -- boolean indicating receiver packet acked
    // packet_present -- boolean indicating a data packet is included
    // packet -- kPacketSize length data packet
    // *** CRC has kConfirmationWord pushed on both sides to accomadate zeroes
    // crc -- crc word
    if (auto win = wireless.TryAcquireInputStream(receive_timer)) {
      bool ack_ = win.GetWordWithCrc();
      packet_present = win.GetWordWithCrc();
      Packet rx_temp_buffer;
      for (auto& element : rx_temp_buffer)
        element = win.GetWordWithCrc();

      if (win.CheckCrc(kCrcConfirmationWord)) {
        if (ack_) 
          tx_buffer.Pop();
        if (packet_present)
          rx_buffer.Push(rx_temp_buffer);
      }
    } else {
      continue;
    }

    auto transmit_timer = MakeSoftTimer(bbu);
    transmit_timer.Start(kWaitForTransmitTime);
    transmit_timer.Wait();

    radio.SwitchToTransmit(0);

    if (auto wout = wireless.TryAcquireOutputStream()) {
      wout.PushWord(packet_present);

      Packet empty_packet{};
      const auto& packet_to_send = tx_buffer.HasData() ? tx_buffer.Peek() :
                                                         empty_packet;
      for (auto element : packet_to_send)
        wout.PushWord(element);

      crc16.Push(kCrcConfirmationWord);
    }
  }
}
