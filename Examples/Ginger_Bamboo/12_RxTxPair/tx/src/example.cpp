/* Author: Sam Cristall
 * License: GPLv3
 * Edited by: Thia Wyrod
 */
#include <Thread.h>
#include "WirelessThreads.h"

void* worker_thread(void* arg)
{
  auto* buffers = reinterpret_cast<WirelessBuffers*>(arg);
  auto& tx_buffer = buffers->tx_buffer;
  Packet send_pkt;
  size_t i = 0;
  while (true) {
    memset(&send_pkt, i, sizeof(send_pkt));
    xpd_puts("\nSending a packet with the following contents:");
    for (auto& element : send_pkt) {
      xpd_putc(' ');
      xpd_echo_int(element, XPD_Flag_Hex);
    }
    while(!tx_buffer.Push(send_pkt)) {
      wait_ms(1000);
    }
    wait_ms(1000);
    ++i;
  }
  return NULL;
}

static WirelessBuffers data_buffers;
void* (*const thrd_funcs[])(void*) = { wireless_thread, worker_thread };

/* Set up and run all secondary threads, then sleep */
int main() {
  sys_clock_init(kCrysFreq, kSystemF);
  xpd_puts("\nBoot successful! Starting tx...");
  size_t curr_thrd_num = 1;
  for (auto thrd_func : thrd_funcs) {
    thread_setup(thrd_func, &data_buffers, curr_thrd_num);
    thread_run(curr_thrd_num);
    ++curr_thrd_num;
  }
  return 0;
}
