#include "CircularBuffer.h"

/* Parameters for the wireless setup - customize to suit your protocol */
static const size_t kPacketSize = 1;
static const size_t kNumPacketsInBuffer = 16;
static const size_t kNumReceivers = 1;
static const uint16_t kTransmitTimeInBbuTicks = 1000;
static const uint16_t kPollCycleTime = 
                          kTransmitTimeInBbuTicks * (1 + kNumReceivers);
static const uint16_t kCrcConfirmationWord = 0xCA47;

using Packet = std::array<uint16_t, kPacketSize>;

/* Convenience struct for packet-storing buffers */
struct WirelessBuffers {
  CircularBuffer<Packet, kNumPacketsInBuffer> tx_buffer;
  CircularBuffer<Packet, kNumPacketsInBuffer> rx_buffer;
};
