/* Author: Sam Cristall
 * License: GPLv3
 * Edited by: Thia Wyrod
 */
#include <array>

#ifndef _CIRC_BUFFER_H_
#define _CIRC_BUFFER_H_
/* Implementation of a simple circular buffer: please note that it is NOT
 * thread-safe as-is, unless a maximum of one thread writes to it, and a maximum
 * of one thread reads from it */
template <typename T, size_t inLen>
class CircularBuffer {
  public:
    CircularBuffer() : read_(0), write_(0) {}

    const T& Peek() {
      return buffer_[read_];
    }

    void Pop() {
      if (read_ != write_)
        read_++;
      if (read_ == Len)
        read_ = 0;
    }

    bool Push(T& data) {
      if (IsFull() == false) {
        buffer_[write_] = data;
        if (++write_ == Len)
          write_ = 0;
        return true;
      }
      return false;
    }

    bool HasData() const {
      return write_ != read_;
    }

    bool IsFull() const {
      size_t end = read_ == 0 ? Len - 1 : read_ - 1;
      return write_ == end;
    }
    
    void Flush() {
      while (HasData())
        Pop();
    }
    
  private:
    static const size_t Len = inLen + 1;
    std::array<T, Len> buffer_;
    size_t read_, write_;
};
#endif //_CIRC_BUFFER_H_
