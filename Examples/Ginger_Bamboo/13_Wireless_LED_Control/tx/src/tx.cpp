/* Author: Thia Wyrod
 * License: GPLv3
 */
#include <Structs.h>
#include <Thread.h>
#include "WirelessThreads.h"

#include "buffered_button.h"
#include "struct_aliases.h"

/* In WirelessConfig.h, we configure the packets to be simply be an array of 1
 * integer, which is a reference to the LED we wish to toggle on or off.
 */

static const size_t kMsPerButtonPoll = 10; /* poll for button input every 10 ms */
static const size_t kMsPerButtonClick = 50; 
static BufferedButton buttons[] = {
  {SW0, kMsPerButtonClick/kMsPerButtonPoll, 0},
  {SW1, kMsPerButtonClick/kMsPerButtonPoll, 0},
  {SW2, kMsPerButtonClick/kMsPerButtonPoll, 0},
  {SW3, kMsPerButtonClick/kMsPerButtonPoll, 0}
};
static const size_t kNumButtons = sizeof(buttons)/sizeof(*buttons);

void* worker_thread(void* arg)
{
  auto* buffers = reinterpret_cast<WirelessBuffers*>(arg);
  auto& tx_buffer = buffers->tx_buffer;
  while (true) {
    for (size_t i = 0; i < kNumButtons; ++i) {
      if(read_buf_button(&buttons[i])) {
        Packet pkt = {{ static_cast<uint16_t>(i) }};
        xpd_puts("\nPushing packet!");
        tx_buffer.Push(pkt);
      }
    }
    wait_ms(kMsPerButtonPoll);
  }
  return NULL;
}

static WirelessBuffers data_buffers;
void* (*const thrd_funcs[])(void*) = { wireless_thread, worker_thread };

/* Set up and run all secondary threads, then sleep */
int main() {
  sys_clock_init(kCrysFreq, kSystemF);
  xpd_puts("\nBoot successful! Starting wireless LED tx...");
  for (const auto& button : buttons) {
    io_set_config(DEFAULT_BUTTON_CFG, button.gl_pin.io_port);
    globalPin_set_dir(PinDir_Input, &button.gl_pin); 
  }
  size_t curr_thrd_num = 1;
  for (auto thrd_func : thrd_funcs) {
    thread_setup(thrd_func, &data_buffers, curr_thrd_num);
    thread_run(curr_thrd_num);
    ++curr_thrd_num;
  }
  return 0;
}
