#ifndef _LIBSPINE_I_BASE_BAND_UNIT_H_
#define _LIBSPINE_I_BASE_BAND_UNIT_H_

#include <cstdint>
#include "Clock.h"
#include "Utility.h"

static const uint16_t PREAMBLE = 0x5555;

INTERFACE(IBaseBandUnit) : public IClock<Concrete> {
public:
  void LockTiming() { STATIC_DISPATCH(LockTiming()); }
  void UnlockTiming() { STATIC_DISPATCH(UnlockTiming()); }
  bool ContainsPreamble() { return STATIC_DISPATCH(ContainsPreamble()); }
  uint16_t PeekWord() { return STATIC_DISPATCH(PeekWord()); }
  uint16_t GetWord() { return STATIC_DISPATCH(GetWord()); }
  void PushWord(uint16_t value) { return STATIC_DISPATCH(PushWord(value)); }
  void SetTransmitting(bool value) { STATIC_DISPATCH(SetTransmitting(value)); }
  uint16_t GetTime() const { return STATIC_DISPATCH_CONST(GetTime()); }
};

#endif
