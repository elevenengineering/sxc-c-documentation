set(CMAKE_BUILD_TYPE "Release" CACHE STRING "Build type...")
set(SXC_CROSS_COMPILE ON)

cmake_minimum_required(VERSION 3.3)
get_filename_component(PROJECT_NAME "${CMAKE_SOURCE_DIR}" NAME)
project(${PROJECT_NAME} NONE)

set (SXC_INLINE_THRESHOLD "15" CACHE STRING
  "Inline threshold for compiling firmware")
set (SXC_UNROLL_THRESHOLD "20" CACHE STRING
  "Unroll threshold for compiling firmware")

find_package(SXC REQUIRED)
include_directories(${SXC_INCLUDE_DIRS})
include(SXCMacros)

set(kMainGen "${PROJECT_SOURCE_DIR}/main.gen")
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -D__BOARD_XINC2_DEV__")
set(AddedDefines "-D" "HW_CFG=devboard")

build_executable(
  ${PROJECT_NAME}
  ${PROJECT_SOURCE_DIR}
  ${PROJECT_BINARY_DIR}
  empty_var
  empty_var)
sxc_firmware_obj_to_hex(
  ${PROJECT_NAME}
  ${kMainGen}
  AddedDefines
  empty_var
  hex_file)
add_custom_target(${PROJECT_NAME}-hex-final ALL
  DEPENDS ${hex_file})
