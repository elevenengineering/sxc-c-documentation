/* Author: Sam Cristall
 * License: GPLv3
 * Edited by: Thia Wyrod
 */
#include <Thread.h>
#include "WirelessThreads.h"

void* worker_thread(void* arg)
{
  auto* buffers = reinterpret_cast<WirelessBuffers*>(arg);
  auto& rx_buffer = buffers->rx_buffer;
  Packet pkt;
  while (true) {
    if (rx_buffer.HasData()) {
      pkt = rx_buffer.Peek();
      rx_buffer.Pop();
      xpd_puts("\nReceived a packet with the following contents:");
      for (auto element : pkt) {
        xpd_putc(' ');
        xpd_echo_int(element, XPD_Flag_Hex);
      }
    }
  }
  return NULL;
}

static WirelessBuffers data_buffers;
void* (*const thrd_funcs[])(void*) = { wireless_thread, worker_thread };

/* Set up and run all secondary threads, then sleep */
int main() {
  sys_clock_init(kCrysFreq, kSystemF);
  xpd_puts("\nBoot successful! Starting rx...");
  size_t curr_thrd_num = 1;
  for (auto thrd_func : thrd_funcs) {
    thread_setup(thrd_func, &data_buffers, curr_thrd_num);
    thread_run(curr_thrd_num);
    ++curr_thrd_num;
  }
  return 0;
}
