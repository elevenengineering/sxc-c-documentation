/* Author: Sam Cristall
 * License: GPLv3
 * Edited by: Thia Wyrod
 */
#include <Structs.h>
#include <Thread.h>
#include "WirelessThreads.h"

static const size_t kWirelessThrdNum = 2;
static const size_t kWorkerThrdNum = 1;
static const GlobalPin trans_inp = { GPIO_A, io_PA, 0x1, Polar_ActiveHigh };
static const GlobalPin recv_inp = { GPIO_A, io_PA, 0x2, Polar_ActiveHigh };
static const GlobalPin *const inps[] = { &trans_inp, &recv_inp };

enum Wireless_Mode {
  None,
  Receiver,
  Transmitter
};

WirelessBuffers data_buffers;

static void switch_wireless_thread(enum Wireless_Mode mode)
{
  thread_stop(kWirelessThrdNum);
  void* (*thrd_func)(void*) = nullptr;
  switch(mode) {
  case None:
    xpd_puts("\nShutting off wireless!");
    return;
  case Transmitter:
    xpd_puts("\nGoing into transmitter mode!");
    thrd_func = transmitter_thread;
    break;
  case Receiver:
    xpd_puts("\nGoing into receiver mode!");
    thrd_func = receiver_thread;
    break;
  }
  thread_setup(thrd_func, (void*)&data_buffers, kWirelessThrdNum);
  data_buffers.tx_buffer.Flush();
  data_buffers.rx_buffer.Flush();
  thread_run(kWirelessThrdNum);
}

/* This function defines the thread that will be responsible for filling and
 * emptying the tx and rx buffers respectively by executing your
 * program/protocol logic.
 */
void* worker_thread(void* arg)
{
  for (size_t i = 0; i < sizeof(inps)/sizeof(*inps); ++i) {
    io_set_config(IO_FAST_SLEW|IO_PULL_ENABLE|IO_PULL_DOWN|IO_DRIVE_4mA,
      inps[i]->io_port);
    globalPin_set_dir(PinDir_Input, inps[i]);
  }
  auto* buffers = reinterpret_cast<WirelessBuffers*>(arg);
  auto& tx_buffer = buffers->tx_buffer;
  auto& rx_buffer = buffers->rx_buffer;
  enum Wireless_Mode mode = Receiver;
  switch_wireless_thread(mode);
  while (true) {
    switch (mode) {
    case None:
      if (globalPin_read(&recv_inp) == ON) {
        mode = Receiver;
        switch_wireless_thread(mode);
      }
      else if (globalPin_read(&trans_inp) == ON) {
        mode = Transmitter;
        switch_wireless_thread(mode);
      }
      break;

    case Transmitter:
      {
        Packet pkt = {{ 2 }};
        xpd_puts("\nSending a packet with the following contents:");
        for (auto element : pkt) {
          xpd_putc(' ');
          xpd_echo_int(element, XPD_Flag_Hex);
        }
        while(bool unpushed = !tx_buffer.Push(pkt))
          unpushed = !tx_buffer.Push(pkt);
        wait_ms(500);
      }
      if (globalPin_read(&recv_inp) == ON) {
        mode = Receiver;
        switch_wireless_thread(mode);
      }
      break;

    case Receiver:
      if (rx_buffer.HasData()) {
        Packet pkt = rx_buffer.Peek();
        rx_buffer.Pop();
        xpd_puts("\nReceived a packet with the following contents:");
        for (auto element : pkt) {
          xpd_putc(' ');
          xpd_echo_int(element, XPD_Flag_Hex);
        }
      }
      if (globalPin_read(&trans_inp) == ON) {
        mode = Transmitter;
        switch_wireless_thread(mode);
      }
      break;
    }
  }
  return NULL;
}

/* Start execution by not having any wireless thread running */
int main() {
  sys_clock_init(kCrysFreq, kSystemF);
  thread_setup(worker_thread, (void*)&data_buffers, kWorkerThrdNum);
  thread_run(kWorkerThrdNum);
  return 0;
}
