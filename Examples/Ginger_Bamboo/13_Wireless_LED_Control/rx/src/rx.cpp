/* Author: Thia Wyrod
 * License: GPLv3
 */
#include <Structs.h>
#include <Thread.h>
#include "WirelessThreads.h"

#include "struct_aliases.h"

/* In WirelessConfig.h, we configure the packets to be simply be an array of 1
 * integer, which is a reference to the LED we wish to toggle on or off.
 */

static GlobalPin const LEDs[] = { LED1, LED2, LED3, LED4 };
static const size_t kNumLEDs = sizeof(LEDs)/sizeof(*LEDs);
static enum PinLogicState LED_states[kNumLEDs] = { OFF };

void* worker_thread(void* arg)
{
  auto* buffers = reinterpret_cast<WirelessBuffers*>(arg);
  auto& rx_buffer = buffers->rx_buffer;
  Packet pkt;
  while (true) {
    if (rx_buffer.HasData()) {
      pkt = rx_buffer.Peek();
      rx_buffer.Pop();
      size_t led_index = pkt[0];
      if (led_index < kNumLEDs) {
        if (LED_states[led_index] == OFF)
          LED_states[led_index] = ON;
        else
          LED_states[led_index] = OFF;
        globalPin_write(LED_states[led_index], &LEDs[led_index]);
      }
      else {
        xpd_puts("\nReceived packet value ");
        xpd_echo_int(led_index, XPD_Flag_UnsignedDecimal);
        xpd_puts(" is out-of-bounds!");
      }
    }
  }
  return NULL;
}

static WirelessBuffers data_buffers;
void* (*const thrd_funcs[])(void*) = { wireless_thread, worker_thread };

/* Set up and run all secondary threads, then sleep */
int main(void)
{
  sys_clock_init(kCrysFreq, kSystemF);
  xpd_puts("\nBoot successful! Starting wireless LED rx...");
  for (const auto& LED : LEDs) {
    io_set_config(DEFAULT_LED_CFG, LED.io_port);
    globalPin_set_dir(PinDir_Output, &LED); 
  }
  size_t curr_thrd_num = 1;
  for (auto thrd_func : thrd_funcs) {
    thread_setup(thrd_func, &data_buffers, curr_thrd_num);
    thread_run(curr_thrd_num);
    ++curr_thrd_num;
  }
  return 0;
}
