/* Author: Sam Cristall
 * License: GPLv3
 */

#include <SXC.h>
#include <XPD.h>
#include "CRC.h"
#include "FEC.h"
#include "GinsengRadio.h"
#include "Wireless.h"
#include "time_funcs.h"
#include "WirelessConfig.h"

/* This function defines the thread that will be responsible for filling and
 * emptying the rx and tx buffers respectively by listening on the radio/BBU and
 * transmitting data. Only modify this if you are feeling brave!
 */
inline void* wireless_thread(void* arg) {
  auto* buffers = reinterpret_cast<WirelessBuffers*>(arg);
  auto& tx_buffer = buffers->tx_buffer;
  auto& rx_buffer = buffers->rx_buffer;

  RadioPort radio_port;
  RadioBbuPort bbu_port;
  RadioSpi spi(kSysFreq);
  RadioBbu bbu(kSysFreq, kRadioStartWord);
  Crc16Lfsr crc16;
  FecLfsr fec;

  auto chip_select =
     MakeOutputPin(radio_port, kRadioChipSelectPinNumber, true);
  auto bbu_pin = MakeOutputPin(bbu_port, kBbuDirectionPinNumber, true);
  auto spi_with_cs = MakeSpiWithChipSelect(spi, chip_select); 
  auto radio = MakeGinsengRadio(spi_with_cs, bbu, radio_port, bbu_pin);
  auto wireless = MakeWireless(bbu, crc16, fec, kRadioStartWord);
  auto poll_cycle_clock = MakeSoftTimer(bbu);
  uint16_t ack_vector = 0, rx_acks = 0;

  auto GetReceiveClock = [&](size_t rx) {
    auto timer = MakeSoftTimer(bbu);
    timer.Start((rx + 1) * kTransmitTimeInBbuTicks);
    return timer;
  };

  while (true) {
    radio.ChangeChannel(0);

    poll_cycle_clock.Wait();

    radio.SwitchToTransmit(0);

    if (auto wout = wireless.TryAcquireOutputStream()) {
      wout.PushWord(ack_vector);
      ack_vector = 0;
      Packet empty_packet{};
      auto& packet_to_send = tx_buffer.HasData() ? tx_buffer.Peek()
                                                 : empty_packet;
      wout.PushWord(tx_buffer.HasData());
      for (auto element : packet_to_send) 
        wout.PushWord(element);

      crc16.Push(kCrcConfirmationWord);
    }

    radio.SwitchToReceive();

    for (size_t i = 0; i < kNumReceivers; ++i) {
      if (auto win = wireless.TryAcquireInputStream(GetReceiveClock(i))) {
        bool ack_;
        ack_ = win.GetWordWithCrc();
        Packet rx_temp_buffer;
        for (auto& element : rx_temp_buffer)
          element = win.GetWordWithCrc();

        if (win.CheckCrc(kCrcConfirmationWord)) {
          if (ack_)
            rx_acks |= (1 << i);
          rx_buffer.Push(rx_temp_buffer);
          ack_vector |= (1 << i);
        }
      }
    }

    if (rx_acks == ((1 << kNumReceivers) - 1)) {
      rx_acks = 0;
      tx_buffer.Pop();
    }

    poll_cycle_clock.Tick(kPollCycleTime);
  }
}
