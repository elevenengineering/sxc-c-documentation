/* Author: Thia Wyrod
 * License: GPLv3
 */
#ifndef _TIME_FUNCS_H_
#define _TIME_FUNCS_H_
#include <SystemClock.h>
#include "SysClock.h"

/* System consts for Ginseng, do not modify */
const enum crystal_freq kCrysFreq = crys_24_576_MHz;
const SystemFrequency kSysFreq = SystemFrequency::_49_152_MHz;
const enum sys_freq kSystemF = _49_152_MHz;

static inline void wait_ms(size_t ms)
{
  for (size_t i = 0; i < ms; ++i) {
    for (size_t i = 0; i < kNumSlicesPerMs; ++i)
      sys_clock_wait(sys_clock_ticks_per_ms_slice(kSystemF));
  }
}
#endif //_TIME_FUNCS_H_
