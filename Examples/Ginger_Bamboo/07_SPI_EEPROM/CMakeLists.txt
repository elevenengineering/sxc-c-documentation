set(CMAKE_BUILD_TYPE "Release" CACHE STRING "Build type...")
set(SXC_CROSS_COMPILE ON)

cmake_minimum_required(VERSION 3.3)
get_filename_component(PROJECT_NAME "${CMAKE_SOURCE_DIR}" NAME)
project(${PROJECT_NAME} NONE)

set (SXC_INLINE_THRESHOLD "15" CACHE STRING
  "Inline threshold for compiling firmware")
set (SXC_UNROLL_THRESHOLD "20" CACHE STRING
  "Unroll threshold for compiling firmware")

find_package(SXC REQUIRED)
include_directories(${SXC_INCLUDE_DIRS})
include(SXCMacros)

set(kMainGen "${PROJECT_SOURCE_DIR}/main.gen")
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -D__BOARD_GINGER__")
set(AddedDefines "-D" "HW_CFG=ginger")

BUILD_EXECUTABLE(${PROJECT_NAME} ${PROJECT_SOURCE_DIR} ${PROJECT_BINARY_DIR})
SXC_FIRMWARE_OBJ_TO_HEX(hex_file ${kMainGen} ${PROJECT_NAME} empty_var
  AddedDefines)
add_custom_target(${PROJECT_NAME}-hex ALL
  DEPENDS ${hex_file})